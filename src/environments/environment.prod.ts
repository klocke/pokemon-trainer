export const environment = {
  production: true,
  trainerAPI: 'https://experis-fall2021-assignments.herokuapp.com/trainers',
  apiKey: 'experis-assignment-api-key',
  sessionName: 'Trainer',
  pokeApi: 'https://pokeapi.co/api/v2/',
};
