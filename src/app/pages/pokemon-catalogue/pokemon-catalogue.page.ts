import { Component, OnInit } from '@angular/core';
import { PokemonService } from '../../services/pokemon.service';
import { Pokemon } from '../../models/pokemon.models';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.page.html',
  styleUrls: ['./pokemon-catalogue.page.css'],
})
export class PokemonCataloguePage implements OnInit {
  get pokemon(): Pokemon[] {
    return this.pokemonService.pokemon.filter(
      (p) => p.name.search(this.filter) != -1
    );
  }

  private filter = '';

  constructor(private pokemonService: PokemonService) {}

  ngOnInit(): void {
    this.pokemonService.fetchPokemonFromApi();
    this.pokemonService.fetchAvailablePokemon();
  }

  onScroll() {}

  filterPokemon(form: NgForm) {
    this.filter = form.value.pokemonSearch.toLowerCase();
  }
}
