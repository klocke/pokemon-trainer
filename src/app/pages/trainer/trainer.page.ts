import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.models';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css'],
})
export class TrainerPage {
  get pokemon(): Pokemon[] {
    return this.sessionService.trainer.pokemon;
  }

  get OwnesPokemon(): boolean {
    return this.sessionService.trainer.pokemon.length > 0;
  }

  constructor(private readonly sessionService: SessionService) {}

  onScroll() {}
}
