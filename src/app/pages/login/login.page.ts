import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { TrainerService } from '../../services/trainer.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css'],
})
export class LoginPage {
  constructor(private readonly trainerService: TrainerService) {}

  onLogin(form: NgForm): void {
    this.trainerService.findByusername(form.value.username);
  }
}
