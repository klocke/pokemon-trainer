import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css'],
})
export class ButtonComponent implements OnInit {
  constructor() {}
  @Input() type: string = '';
  @Input() disable: boolean = false;
  @Input() icon: string = 'Go!';
  @Input() color: string = '#845EC2';
  ngOnInit(): void {}

  @Output() clickEmitter: EventEmitter<any> = new EventEmitter<any>();
  public onClick() {
    this.clickEmitter.emit();
  }
}
