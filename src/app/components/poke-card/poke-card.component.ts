import { Component, Input, OnInit } from '@angular/core';
import { SessionService } from 'src/app/services/session.service';
import { TrainerService } from 'src/app/services/trainer.service';
import { errorPokemon, Pokemon } from '../../models/pokemon.models';

@Component({
  selector: 'app-poke-container',
  templateUrl: './poke-card.component.html',
  styleUrls: ['./poke-card.component.css'],
})
export class PokeCardComponent implements OnInit {
  namePlateClass: string = 'poke-card-container notOwned';

  constructor(
    private readonly sessionService: SessionService,
    private readonly trainerService: TrainerService
  ) {}

  @Input() pokemon: Pokemon = errorPokemon;

  onClick(): void {
    if (this.namePlateClass === 'poke-card-container owned') {
      this.sessionService.removeFromCollection(this.pokemon);
      this.namePlateClass = 'poke-card-container notOwned';
    } else {
      this.sessionService.addToCollection(this.pokemon);
      this.namePlateClass = 'poke-card-container owned';
    }

    this.trainerService.Update(
      this.sessionService.trainer.id,
      this.sessionService.trainer.pokemon
    );
  }

  ngOnInit(): void {
    if (
      this.sessionService.trainer.pokemon.filter(
        (pokemon) => pokemon.name === this.pokemon.name
      ).length > 0
    ) {
      this.namePlateClass = 'poke-card-container owned';
    }
  }
}
