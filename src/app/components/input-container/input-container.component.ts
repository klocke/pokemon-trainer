import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';

@Component({
  selector: 'app-input',
  templateUrl: './input-container.component.html',
  styleUrls: ['./input-container.component.css'],
})
export class InputContainerComponent {
  @Output() onSubmitFunction: EventEmitter<NgForm> = new EventEmitter<NgForm>();
  @Input() placeHolder: string = '';
  @Input() name: string = '';
  @Input() id: string = '';
  @Input() icon: string = '';
  @Input() required: boolean = true;
  @Input() minlength: string = '2';
  public onSubmit(form: NgForm): void {
    this.onSubmitFunction.emit(form);
  }
}
