import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent {
  @Input() headerTitle: string | undefined;

  get trainerLoggedIn() {
    return this.sessionService.trainer.loggedIn;
  }

  get trainerName() {
    return this.sessionService.trainer.username;
  }

  get onTrainerPage() {
    return this.router.url === '/trainer';
  }

  constructor(
    private readonly router: Router,
    private readonly sessionService: SessionService
  ) {}

  onHeaderTitleClick(): void {
    this.router.navigate(['pokemon-catalogue']);
  }

  onProfileButtonClick() {
    this.router.navigate(['trainer']);
  }

  onLogoutButtonClick() {
    if (confirm('Are you sure you want to logout?')) {
      this.sessionService.logout();
      this.router.navigate(['login']);
    }
  }
}
