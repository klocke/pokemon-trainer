import { Component } from '@angular/core';

@Component({
  selector: 'app-loading',
  template: '<div id="loader"></div>',
  styleUrls: ['./loading.component.css'],
})
export class LoadingComponent {}
