export interface Pokemon {
  id: number;
  name: string;
  url: string;
  avatar: string;
}

export const errorPokemon: Pokemon = {
  id: -1,
  name: '',
  url: '',
  avatar: '',
};
