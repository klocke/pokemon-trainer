import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { SessionService } from '../services/session.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(
    private readonly sessionService: SessionService,
    private readonly router: Router
  ) {}

  canActivate(route: ActivatedRouteSnapshot): boolean {
    if (route.data.ShouldBeAuthorized) {
      if (this.sessionService.trainer.loggedIn) {
        return true;
      }

      this.router.navigate(['login']);
      return false;
    } else {
      if (this.sessionService.trainer.loggedIn) {
        this.router.navigate(['pokemon-catalogue']);
        return false;
      }

      return true;
    }
  }
}
