import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Pokemon, errorPokemon } from '../models/pokemon.models';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class PokemonService {
  constructor(private readonly http: HttpClient) {}

  private _pokemon: Pokemon[] = [];
  private _error: string = '';

  get pokemon(): Pokemon[] {
    return this._pokemon;
  }
  set pokemon(value: Pokemon[]) {
    this._pokemon = value;
  }
  private addPokemon(value: Pokemon): void {
    // Only add if not in list already
    if (this._pokemon.find((p: Pokemon) => p.id === value.id)) {
      return;
    }
    this._pokemon.push(value);
  }

  private static dtoToPokemon(dto: any): Pokemon {
    const urlArr = dto.url.split('/');
    const id = Number(urlArr[urlArr.length - 2]);
    const avatar = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${id}.png`;
    return <Pokemon>{ id: id, url: dto.url, name: dto.name, avatar: avatar };
  }

  public fetchPokemonFromApi(limit: number = 500): void {
    const url = `${environment.pokeApi}pokemon?limit=${limit}`;

    this.http
      .get<any>(url)
      .pipe(
        map((response) =>
          response.results.map((pokeDto: any) =>
            PokemonService.dtoToPokemon(pokeDto)
          )
        )
      )
      .subscribe(
        (pokemons: Pokemon[]) => {
          for (const pokemon of pokemons) {
            this.addPokemon(pokemon);
          }

          this.saveAvailablePokemon();
        },
        (e: HttpErrorResponse) => console.log(e.message)
      );
  }

  saveAvailablePokemon(): void {
    localStorage.setItem('POKEMON_AVAILABLE', JSON.stringify(this._pokemon));
  }

  fetchAvailablePokemon(): Pokemon[] {
    const pokemon = localStorage.getItem('POKEMON_AVAILABLE');
    if (pokemon != null) {
      return JSON.parse(pokemon);
    }

    return [errorPokemon];
  }

  set error(value: string) {
    this._error = value;
  }
}
