import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.models';
import { Trainer } from '../models/trainer.models';
import { SessionService } from './session.service';

@Injectable({
  providedIn: 'root',
})
export class TrainerService {
  private _error: string = '';

  constructor(
    private readonly http: HttpClient,
    private readonly sessionService: SessionService
  ) {}

  public error(): string {
    return this._error;
  }

  public findByusername(username: string): void {
    this.sessionService.setisLoading(true);
    this.http
      .get<Trainer[]>(`${environment.trainerAPI}?username=${username}`)
      .subscribe(
        (trainers: Trainer[]) => {
          if (trainers[0]) {
            this.sessionService.setTrainer(trainers[0]);
            return;
          }

          this.create(username);
        },
        (error: HttpErrorResponse) => {
          this._error = error.message;
        }
      );
  }

  public create(username: string): void {
    const httpOptions = {
      headers: new HttpHeaders({
        'X-API-Key': environment.apiKey,
        'Content-Type': 'application/json',
      }),
    };

    this.http
      .post<Trainer>(
        environment.trainerAPI,
        { username, pokemon: [] },
        httpOptions
      )
      .subscribe(
        (trainer: Trainer) => this.sessionService.setTrainer(trainer),
        (error: HttpErrorResponse) => {
          this._error = error.message;
        }
      );
  }

  public Update(id: number, pokemons: Pokemon[]): void {
    const httpOptions = {
      headers: new HttpHeaders({
        'X-API-Key': environment.apiKey,
        'Content-Type': 'application/json',
      }),
    };

    this.http
      .patch<Trainer>(
        `${environment.trainerAPI}/${id}`,
        { pokemon: pokemons },
        httpOptions
      )
      .subscribe(
        (trainer: Trainer) => this.sessionService.setTrainer(trainer),
        (error: HttpErrorResponse) => {
          this._error = error.message;
        }
      );
  }
}
