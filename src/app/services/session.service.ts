import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.models';
import { Trainer } from '../models/trainer.models';

@Injectable({
  providedIn: 'root',
})
export class SessionService {
  trainerInitialState: Trainer = {
    id: -1,
    username: '',
    pokemon: [],
    loggedIn: false,
  };

  private _trainer: Trainer = this.trainerInitialState;
  private _isLoading: boolean = false;

  constructor(private readonly router: Router) {
    const storedTrainer = localStorage.getItem(environment.sessionName);
    if (storedTrainer) {
      this._trainer = JSON.parse(storedTrainer) as Trainer;
      this._trainer.loggedIn = true;
    }
  }

  get trainer(): Trainer {
    return this._trainer;
  }

  get isLoading(): boolean {
    return this._isLoading;
  }

  public setTrainer(trainer: Trainer): void {
    trainer.loggedIn = true;
    this._trainer = trainer;
    localStorage.setItem(environment.sessionName, JSON.stringify(trainer));
    this.setisLoading(false);
    if (this.router.url === '/login') {
      this.router.navigate(['pokemon-catalogue']);
    }
  }

  public addToCollection(p: Pokemon) {
    if (this._trainer.pokemon.find((pokemon: Pokemon) => pokemon.id === p.id)) {
      return;
    }

    this._trainer.pokemon.push(p);
  }

  public removeFromCollection(p: Pokemon) {
    if (this._trainer.pokemon.find((pokemon: Pokemon) => pokemon.id === p.id)) {
      this._trainer.pokemon = this._trainer.pokemon.filter(
        (pokemon: Pokemon) => pokemon.id !== p.id
      );
    }
  }

  public logout() {
    this._trainer = this.trainerInitialState;
    localStorage.removeItem(environment.sessionName);
  }

  public setisLoading(isLoading: boolean) {
    this._isLoading = isLoading;
  }
}
