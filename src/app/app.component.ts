import { Component } from '@angular/core';
import { SessionService } from './services/session.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  get isLoading() {
    return this.sessionService.isLoading;
  }

  constructor(private readonly sessionService: SessionService) {}

  title = 'pokemon-trainer';
}
