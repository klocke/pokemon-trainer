# Pokémon Trainer

Pokémon Trainer is an example project that shows some Angular fundamentals used to create a simple app showing Pokémons - with "login", favorites and profile.

You can interact with the example here: **[https://tranquil-springs-51433.herokuapp.com/login](https://tranquil-springs-51433.herokuapp.com/login)**

## Prerequisites

Before you begin, ensure you have met the following requirements:

- You have visual studio code installed (or similar text editor/IDE)
- You have node and npm installed

## Installing Pokémon Trainer

To install Pokémon Trainer, follow these steps:

Open git bash and navigate to the desired location

Paste:
`git clone git@gitlab.com:klocke/pokemon-trainer.git`

`cd pokemon-trainer`

`npm install`

## Using Pokémon Trainer

To use Pokémon Trainer, follow these steps:

Open the solution in your IDE and run the project.

A websie will open up and you will be able to:

1. Enter a username - will create a new user, if the username cannot be found in the database
2. You are taken to the Pokémon catalog where you can click a Pokémon to "catch it" (favorite)
3. You can click your avatar in the top right. You will be able to choose your profile or to logout
4. The profile shows the Pokémons the trainer has caugt

## Contributing to Pokémon Trainer

To contribute to Pokémon Trainer, follow these steps:

1. Fork this repository.
2. Create a branch: `git checkout -b <branch_name>`.
3. Make your changes and commit them: `git commit -m '<commit_message>'`
4. Push to the original branch: `git push origin https://gitlab.com/klocke/pokemon-trainer`
5. Create the pull request.

Alternatively see the GitHub documentation on [creating a pull request](https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/creating-a-pull-request).

## Contributors

Thanks to the following people who have contributed to this project:

You name will go here if you contribute. :)

## Contact

Please make an issue with your request.

## License

This project uses the following license: [MIT](https://choosealicense.com/licenses/mit/).
